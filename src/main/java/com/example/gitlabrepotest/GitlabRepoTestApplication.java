package com.example.gitlabrepotest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabRepoTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabRepoTestApplication.class, args);
	}

}
